'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ProfileSchema = new Schema({
  userId: {
  	type: String
  },
  firstName: {
  	type: String,
  	min: 1,
  	max: 25
  },
  middleName: {
  	type: String,
  	min: 1,
  	max: 25
  },
  lastName: {
  	type: String,
  	min: 1,
  	max: 25
  },
  dob: {
  	type: Date
  },
  birthPlace: {
  	type: String,
  	min: 2,
  	max: 25
  },
  fatherName: {
  	type: String,
  	min: 2,
  	max: 25
  },
  motherName: {
  	type: String,
  	min: 2,
  	max: 25
  },
  mobileNumber: {
  	type: String,
  	min: 10,
  	max: 10
  },
  city: {
  	type: String
  },
  country: {
  	type: String
  },
  active: {
  	type: Boolean,
  	default: true
  }
});

module.exports = mongoose.model('Profile', ProfileSchema);