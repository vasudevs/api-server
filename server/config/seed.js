/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
// Insert seed models below
var Profile = require('../api/profile/profile.model');
var Thing = require('../api/thing/thing.model');
var User = require('../api/user/user.model');

// Insert seed data below
var profileSeed = require('../api/profile/profile.seed.json');
var thingSeed = require('../api/thing/thing.seed.json');

// Insert seed inserts below
Profile.find({}).remove(function() {
	Profile.create(profileSeed);
});

Thing.find({}).remove(function() {
  Thing.create(thingSeed);
});